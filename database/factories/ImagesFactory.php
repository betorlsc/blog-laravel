<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Images>
 */
class ImagesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'url' => 'posts/' . $this->faker->image(
                dir: storage_path('app/public/posts'),
                width: 640,
                height: 480, category: null, fullPath: false 
                )
        ];
    }
}


