@extends('adminlte::page')

@section('title', 'Rol')

@section('content_header')
    @can('admin.roles.create')
    <a href="{{ route('admin.roles.create') }}" class="btn btn-secondary btn-sm float-right">Nuevo rol</a>
    @endcan
    <h1>Lista de roles </h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif
    <div class="card">


        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Role</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($roles as $role)
                        <tr>
                            <td>{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                            <td width="10px">
                                 @can('admin.roles.edit')
                                <a href="{{ route('admin.roles.edit', $role) }}" class="btn btn-primary btn-sm">
                                    Editar
                                </a>
                                  @endcan
                            </td>
                            <td width="10px">
                                 @can('admin.roles.destroy')
                                <form action="{{ route('admin.roles.destroy', $role) }}" method="post">


                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-danger btn-sm" type="submit"
                                        onclick="return confirm('Desea eliminar ?')">Eliminar</button>
                                </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
