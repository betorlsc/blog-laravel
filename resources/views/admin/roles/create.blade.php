@extends('adminlte::page')

@section('title', 'Rol')

@section('content_header')
    <h1>Crear nueva rol</h1>
@stop

@section('content')

    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'admin.roles.store']) !!}

            
            @include('admin.roles.partials.form')
            {!! form::submit('Crear rol', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('js')
  
@endsection
