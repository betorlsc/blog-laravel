@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@can('admin.tags.create')
<a href="{{ route('admin.tags.create') }}" class="btn btn-secondary btn-sm float-right">Agregar Etiqueta</a>
@endcan
    <h1>Lista de etiqueta</h1>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif
    <div class="card">

       
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Color</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <td>{{ $tag->id }}</td>
                            <td>{{ $tag->name }}</td>
                            <td><div class=" px-3 h-6  text-{{ $tag->color }} ">{{ $tag->color }}</div></td>
                            <td width="10px">
                               @can('admin.tags.edit')
                               <a href="{{ route('admin.tags.edit', $tag) }}" class="btn btn-primary btn-sm">
                                Editar
                            </a>
                               @endcan
                            </td>
                            <td width="10px">
                                @can('admin.tags.destroy')
                                <form action="{{ route('admin.tags.destroy', $tag) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button class="btn btn-danger btn-sm" type="submit"
                                        onclick="return confirm('Desea eliminar ?')">Eliminar</button>
                                </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
