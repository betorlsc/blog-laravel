<div class="form-group">
    {!! Form::label('name', 'Nombre') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el nombre de la post']) !!}
    @error('name')
        <span class="text-danger">{{ $message }}</span>
    @enderror

</div>

<div class="form-group">
    {!! Form::label('slug', 'Slug') !!}
    {!! Form::text('slug', null, [
        'class' => 'form-control',
        'placeholder' => 'Ingrese el slug de la post',
        'readonly',
    ]) !!}
    @error('slug')
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>

<div class="form-group">
    {!! Form::label('category_id', 'Categor�as') !!}
    {!! Form::select('category_id', $categories, null, [
        'class' => 'form-control',
        'placeholder' => 'Elige una categor�a',
    ]) !!}
    @error('category_id')
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>


<div class="form-group">
    <p class="font-weight-bold">Etiqueta </p>
    @foreach ($tags as $tag)
        <label class="mr-2">
            {!! Form::checkbox('tags[]', $tag->id, null) !!}
            {{ $tag->name }}
        </label>
    @endforeach
    @error('tags')
        <br>
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>

<div class="form-group">
    <p class="font-weight-bold">Estado </p>
    <label class="mr-2">
        {!! Form::radio('status', 1, true) !!}
        Borrador
    </label>
    <label class="mr-2">
        {!! Form::radio('status', 2, false) !!}
        Publicado
    </label>
    @error('status')
        <br>
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>

<div class="row mb-3">
    <div class="col">
        <div class="image-wrapper">

            @isset($post->image)
                <img id="muestra" src="{{ Storage::url($post->image->url) }}" alt="" srcset="">
            @else
                <img id="muestra" src="https://cdn.pixabay.com/photo/2022/01/26/12/01/hut-6968718_960_720.png"
                    alt="">
            @endisset

        </div>

    </div>
    <div class="col">
        <div class="from-group">
            {!! Form::label('file', 'Imagen que se mostrar� en el post') !!}

            {!! Form::file('file', ['class' => 'form-contro-file', 'accept' => 'image/*']) !!}
        </div>
        @error('file')
            <br>
            <span class="text-danger">{{ $message }}</span>
        @enderror
        <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae aspernatur commodi accusantium tempore
            ab harum aliquid temporibus ex nam rerum, quis reprehenderit fugit blanditiis eligendi reiciendis in
            et adipisci cupiditate.
        </p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('extract', 'Extracto') !!}
    {!! Form::textarea('extract', null, ['class' => 'form-control']) !!}
    @error('extract')
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>


<div class="form-group">
    {!! Form::label('body', 'Cuerpo del post') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
    @error('body')
        <span class="text-danger">{{ $message }}</span>
    @enderror
</div>
