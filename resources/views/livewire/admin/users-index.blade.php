<div>
    {{-- Close your eyes. Count to one. That is how long forever feels. --}}

    <div class="card">
        <div class="card-header">
            <input wire:model="search" class="form-control" type="text" name="" id=""
                placeholder="Ingrese nombre o correo de usuario ">
        </div>

        @if ($users->count())
            <div class="card-body">
                <table class="table table-striped">

                    <thead>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th colspan=2></th>

                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td width="10px">
                                    <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary btn-sm">
                                        Editar
                                    </a>
                                </td>
                              
                            </tr>
                        @endforeach
                    </tbody>

                </table>

            </div>
            <div class="card-footer">
                {{ $users->links() }}
            </div>
        @else
            <div class="card-body">
                <strong>
                    No hay ningun regostro ...
                </strong>
            </div>


        @endif
    </div>
</div>
