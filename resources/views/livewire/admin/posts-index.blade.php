<div class="card">

    <div class="card-header">
        <input wire:model="search" class="form-control" type="text" name="" id="">
    </div>
    @if ($posts->count())
        <div class="card-body">
            <table class="table table-striped">

                <thead>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th colspan=2></th>

                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->name }}</td>
                            <td width="10px">
                                <a href="{{ route('admin.posts.edit', $post) }}" class="btn btn-primary btn-sm">
                                    Editar
                                </a>
                            </td>
                            <td width="10px">
                                <form action="{{ route('admin.posts.destroy', $post) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" type="submit"
                                        onclick="return confirm('Desea eliminar ?')">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>

        </div>
        <div class="card-footer">
            {{ $posts->links() }}
        </div>
    @else
        <div class="card-body">
            <strong>
                No hay ningun regostro ...
            </strong>
        </div>


    @endif


</div>
