<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

   public  $colors =  [
        'red' => 'Color Red',
        'yellow' => 'Color Yellow',
        'green' => 'Color Green',
        'blue' => 'Color Blue',
        'indigo' => 'Color Indigo',
        'purple' => 'Color Purple',
        'cyan' => 'Color Cyan',
        'kids' => 'Color Kids',
        'teal' => 'Color Teal'
    ];


    protected $fillable =[
        'name', 'color','slug'
    ];

    public function getRouteKeyName()
    {
        return "slug";

    }
    public function posts(){
        return $this->belongsToMany(Post::class);
    }
}
