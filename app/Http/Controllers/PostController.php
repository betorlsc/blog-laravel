<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
class PostController extends Controller
{
   
    public function index()
    {
        if (request()->page) {
           $key ='posts' . request()->page;
        }else {
            $key ='posts';
        }
        if (Cache::has($key)) {
            $posts = Cache::get($key)    ;
        } else {
            $posts = Post::where('status',2)->latest('id')->paginate(8);
            Cache::put($key, $posts);
        }
        return view('posts.index', compact('posts'));
    }

    public function category(Category $category)
    
    {
        $posts = Post::where('category_id', $category->id)
        ->where('status',2)->latest('id')->paginate(6);

        return view('posts.category', compact('posts', 'category'));
    }


    public function tag(Tag $tag)
    
    {
        $posts = $tag->posts()->where('status',2)->latest('id')->paginate(6);

        return view('posts.tag', compact('posts', 'tag'));
    }

    public function create()
    {
        //
    }


    public function store(PostRequest $request)
    {
        //
    }


    public function show(Post $post)
    {
        $this->authorize('published', $post);
        $similares = Post::where('category_id', $post->category_id)
        ->where('status',2)
        ->where('id', '!=', $post->id) 
        ->latest('id')
        ->take(4)
        ->get();
        return view('posts.show', compact('post', 'similares'));
    }

  
    public function edit(Post $post)
    {
        //
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        //
    }

 
    public function destroy(Post $post)
    {
        //
    }
}
