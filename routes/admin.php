<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\admin\RoleController;


Route::get('', [HomeController::class, 'index'] )->middleware('can:admin.home')->name('admin.home');

Route::resource('users', UserController::class)->only(['index','edit','update'])->names('admin.users');
Route::resource('roles', RoleController::class)->Except('show')->names('admin.roles');

Route::resource('categories', CategoryController::class)->Except('show')->names('admin.categories');
Route::resource('tags', TagController::class)->Except('show')->names('admin.tags');
Route::resource('posts', PostController::class)->Except('show')->names('admin.posts');